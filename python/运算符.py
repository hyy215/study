# ** 幂
print(2 ** 2)    # 4

# // 向下取整除
print(9 // 2)    # 4

# := 海象运算符, 同时进行赋值和返回赋值的值
a = [10,11]
if (n := len(a)) > 1:
    print(f"List is too long ({n} elements, expected <= 1)")

# 位运算符
a = 60            # 60 = 0011 1100 
b = 13            # 13 = 0000 1101 
c = 0

# & 按位与
print(a & b)     # 12 = 0000 1100

# | 按位或
print (a | b)    # 61 = 0011 1101

# ^ 异或，两位相异，结果为 1
print(a ^ b)     # 49 = 0011 0001

# ~ 取反
print(~a)        # -61 = 1100 0011

# << 左移，高位丢弃，低位补 0
print(a << 2)    # 240 = 1111 0000

# >> 右移
print(a >> 2)    # 15 = 0000 1111


# 逻辑运算符
# and 
print(True and False)   # False

# or
print(True or False)    # True

# not 
print(not True)         # False


# 成员运算符 in, not in
list = [1,2,3]
print(1 in list)   # True
print(1 not in list)  # False


# 身份运算符，is(类似 id(x) == id(y) ) ,is not, 返回是否返回同一个对象
# is 与 == 区别：
# is 用于判断两个变量引用对象是否为同一个， == 用于判断引用变量的值是否相等。
a = 20
b = 20
print(a is b)           # True
print(id(a))            # 140729656671256
print(id(a) == id(b))   # True

b = 30;
print(a is b)           # False
print(id(a) == id(b))   # False