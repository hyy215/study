# 多变量赋值
a = b = c = 1
print(a, b, c)

# 标准数据类型：Number、String、bool、List、Tuple、Set、Dictionary
# 不可变数据：Number、String、Tuple
# 可变数据：List、Set、Dictionary

# 数字类型：int、float、bool、complex
number1 = 1       # init
number2 = 1.1     # float
number3 = True    # bool
number4 = 1 + 2j  # complex
# type，查询变量类型
print(type(number4))
# isinstance，判断类型
print(isinstance(number4, complex))

# type、isinstance 区别
# type 不会认为子类是一种父类类型
# isinstance 认为子类是一种父类类型
class A:
    pass

class B(A):
    pass

print(isinstance(A(), A))   # True
print(type(A()) == A)       # True
print(isinstance(B(), A))   # True
print(type(B()) == A)       # False
print(type(B()) == B)       # True

# bool 是 int 的子类
# True 和 False 可以和数字相加
# True==1、False==0 会返回 True，但可以通过 is 来判断类型
print(issubclass(bool, int)) # True
print(True + 1)              # 2
print(True == 1)             # True
print(False == 0)            # True
print(1 is True)             # False
print(True is 1)             # False

# 除法
print(2 / 4)       # 得到一个浮点数，0.5
print(2 // 4)      # 得到一个整数，0

# 乘方
print(2 ** 4)    # 16

# 多变量复制
a, b = 1, 2
print(a, b)

# 在混合计算时，Python 会把整型转换成为浮点数