# Dictionary 字典是无序的对象集合

dict = {}
dict['one'] = "1 - 菜鸟教程"
dict[2]     = "2 - 菜鸟工具"
tinydict = {'name': 'runoob','code':1, 'site': 'www.runoob.com'}

print (dict['one'])       # 1 - 菜鸟教程
print (dict[2])           # 2 - 菜鸟工具
print (tinydict)          # {'name': 'runoob', 'code': 1, 'site': 'www.runoob.com'}
print (tinydict.keys())   # dict_keys(['name', 'code', 'site'])
print (tinydict.values()) # dict_values(['runoob', 1, 'www.runoob.com'])

# 输出可打印的
print(str(dict))    # {'one': '1 - 菜鸟教程', 2: '2 - 菜鸟工具'}    

#  fromkeys() 函数用于创建一个新字典，以序列 seq 中元素做字典的键，value 为字典所有键对应的初始值
seq = ('name', 'age', 'sex')
tinydict = dict.fromkeys(seq, 10)
print(tinydict)   # {'name': 10, 'age': 10, 'sex': 10}

# 以列表返回视图对象
print (tinydict.items())   # dict_items([('name', 10), ('age', 10), ('sex', 10)])

# 更新
tinydict = {'Name': 'Runoob', 'Age': 7}
tinydict2 = {'Sex': 'female' }
 
tinydict.update(tinydict2)
print (tinydict)   # {'Name': 'Runoob', 'Age': 7, 'Sex': 'female'}

# 删除
print(tinydict.pop('Sex'))   # female

#  popitem() 方法随机返回并删除字典中的最后一对键和值
result = tinydict.popitem()
print(tinydict)   # {'Name': 'Runoob'}
print(result)     # ('Age', 7)