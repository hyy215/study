# Bool

a = True
b = False

# 比较运算符
print(2 < 3)   # True
print(2 == 3)  # False

# 逻辑运算符
print(a and b)  # False
print(a or b)   # True
print(not a)    # False

# 类型转换
print(int(a))   # 1
print(float(b)) # 0.0
print(str(a))   # "True"

# 所有非零的数字和非空的字符串、列表、元组等数据类型都被视为 True
# 只有 0、空字符串、空列表、空元组等被视为 False
# 因此，在进行布尔类型转换时，需要注意数据类型的真假性

list  = [];
if list:
    print('true！！！')   # 打印