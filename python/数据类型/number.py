import math
import random

# 数学函数
print(abs(-10))	            # 10, 返回数字的绝对值
print(math.fabs(-10))	    # 10.0, 以浮点数形式返回数字的绝对值
print(math.ceil(4.1))	    # 5, 返回数字的上入整数
print(math.floor(4.9))	    # 4, 返回数字的下舍整数
print(math.exp(1))	        # 2.718281828459045, 返回 e 的 x 次幂(ex)
print(math.log(100, 10))	# 2.0, 返回x的自然对数
print(math.log(math.e))	    # 1.0
print(math.log10(100))	    # 2.0, 返回以10为基数的x的对数
print(max(1, 5))	        # 5, 返回给定参数的最大值，参数可以为序列。
print(min(1, 5))	        # 1, 返回给定参数的最小值，参数可以为序列。
print(math.modf(-1.5))	    # (-0.5, -1.0), 返回x的整数部分与小数部分，两部分的数值符号与输入数相同，整数部分以浮点型表示。
print(pow(2, 3))	        # 8, x**y 运算后的值。
print(round(1.134, 2))	    # 1.13, round(x [,n]), 返回浮点数 x 的四舍五入值，如给出 n 值，则代表舍入到小数点后的位数。其实准确的说是保留值将保留到离上一位更近的一端。
print(math.sqrt(4))	        # 2.0, 返回数字x的平方根。

# 随机函数
# 从序列的元素中随机挑选一个元素
print(random.choice([1,2,3]))	

# 从指定范围内，按指定基数递增的集合中获取一个随机数
# start -- 指定范围内的开始值，包含在范围内, 基数默认值为 1
# stop -- 指定范围内的结束值，不包含在范围内。
# step -- 指定递增基数。
# randrange([start,] stop [,step]) 
print(random.randrange(9, 11, 2))  # 9

# random()	 # 随机生成下一个实数，它在[0,1)范围内。
print(random.random())

# 改变随机数生成器的种子 seed
# 预先使用 random.seed(x) 设定好种子之
# 其中的 x 可以是任意数字，如10
# 这个时候，先调用它的情况下，使用 random() 生成的随机数将会是同一个
random.seed(10)    
print (random.random())    # 0.5714025946899135
random.seed(10)
print (random.random())    # 0.5714025946899135

random.seed("hello",2)
print (random.random())    # 0.3537754404730722
random.seed("hello",2)
print (random.random())    # 0.3537754404730722

# 将序列的所有元素随机排序
list = [1,2,3,4]
random.shuffle(list)
print(list)    # [3, 2, 4, 1]

# 随机生成下一个实数，它在[x,y]范围内。
print(random.uniform(1,2))   # 1.0510866919659994


# 三角函数
# acos(x)	返回x的反余弦弧度值。
# asin(x)	返回x的反正弦弧度值。
# atan(x)	返回x的反正切弧度值。
# atan2(y, x)	返回给定的 X 及 Y 坐标值的反正切值。
# cos(x)	返回x的弧度的余弦值。
# hypot(x, y)	返回欧几里德范数 sqrt(x*x + y*y)。
# sin(x)	返回的x弧度的正弦值。
# tan(x)	返回x弧度的正切值。
# degrees(x)	将弧度转换为角度,如degrees(math.pi/2) ， 返回90.0
# radians(x)	将角度转换为弧度


# 数学常量
# pi    圆周率，一般以π来表示
# e     数学常量 e，e即自然常数（自然常数）