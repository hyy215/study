'''
隐式类型转换
在隐式类型转换中，Python 会自动将一种数据类型转换为另一种数据类型
以下实例中，我们对两种不同类型的数据进行运算，较低数据类型（整数）就会转换为较高数据类型（浮点数）以避免数据丢失
'''

num_int = 123
num_flo = 1.23

num_new = num_int + num_flo

print("num_int 数据类型为:", type(num_int))    # num_int 数据类型为: <class 'int'>
print("num_flo 数据类型为:", type(num_flo))    # num_flo 数据类型为: <class 'float'>

print("num_new 值为:", num_new)                # num_new 值为: 124.23
print("num_new 数据类型为:", type(num_new))    # num_new 数据类型为: <class 'float'>

# 显示转换
# int(x [,base])   将 x 转换为一个整数，base 表示 x 是什么进制
print(int(2.8))         # 2
print(int('12', 16))    # 如果是带参数 base 的话，x 要以字符串的形式进行输入，此处表示 12 为 16进制
print(int('0x0a', 16))  # 10

# float(x)  将x转换到一个浮点数
print(float(1))         # 1.0
print(float('-123.6'))       # -123.6

# complex(real [,imag]) 创建一个复数
print(complex(1, 2))    # (1+2j)
print(complex("1+2j"))  # (1+2j), 注意：这个地方在"+"号两边不能有空格，否则会报错

# real -- int, long, float或字符串；
# imag -- int, long, float；

# str(x)  将对象 x 转换为字符串

# repr(x) 将对象 x 转换为表达式字符串

# eval(str) 用来计算在字符串中的有效Python表达式,并返回一个对象

# tuple(s) 将序列 s 转换为一个元组

# list(s) 将序列 s 转换为一个列表

# set(s) 转换为可变集合

# dict(d) 创建一个字典。d 必须是一个 (key, value)元组序列

# frozenset(s) 转换为不可变集合

# chr(x)  将一个整数转换为一个字符

# ord(x)  将一个字符转换为它的整数值 

# hex(x)  将一个整数转换为一个十六进制字符串

# oct(x)  将一个整数转换为一个八进制字符串
