# Set

# 表示
set1 = {1, 2, 2}
print(set1)   # {1, 2}

set2 = set('111122222')
print(set2)  # {'2', '1'}

# 成员测试
if '2' in set2 :
    print('Runoob 在集合中')
else :
    print('Runoob 不在集合中')
 
# set可以进行集合运算
a = set('abracadabra')
b = set('alacazam')
print(a)   # {'a', 'd', 'c', 'b', 'r'}
print(b)   # {'z', 'm', 'c', 'l', 'a'}

# a 和 b 的差集
print(a - b)     # {'b', 'r', 'd'}

# a 和 b 的并集
print(a | b)     # {'z', 'l', 'c', 'b', 'm', 'd', 'r', 'a'}

# a 和 b 的交集
print(a & b)     # {'c', 'a'}

# a 和 b 中不同时存在的元素
print(a ^ b)     # {'z', 'd', 'm', 'r', 'b', 'l'}


set1 = {1,2,3,4}
# 添加
set1.add(5)
print(set1)   # {1,2,3,4,5}

set1.update({6,7})  # {1, 2, 3, 4, 5, 6, 7}
print(set1)
set1.update([6,9], [11, 12])
print(set1)  # {1, 2, 3, 4, 5, 6, 7, 9, 11, 12}

# 移除
set1.remove(12)
print(set1) #  {1, 2, 3, 4, 5, 6, 7, 9, 11}

# discard 移除集合中的元素，且如果元素不存在，不会发生错误
set1.discard(123)
set1.discard(11)
print(set1) #  {1, 2, 3, 4, 5, 6, 7, 9}

# 随机删除集合中的一个元素
x = set1.pop()
print(x, set1) #  1 {2, 3, 4, 5, 6, 7, 9}