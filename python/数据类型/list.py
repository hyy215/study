import operator

# List
list1 = [ 'abcd', 786 , 2.23, 'runoob', 70.2 ]  # 定义一个列表
tinylist = [123, 'runoob']

# 实例
print (list1)            # ['abcd', 786, 2.23, 'runoob', 70.2]
print (list1[0])         # abcd
print (list1[1:4:2])       # [786, 'runoob']
print (list1[2:])        # [2.23, 'runoob', 70.2]

# * 重复操作
print (tinylist * 2)    # [123, 'runoob', 123, 'runoob']

# + 连接
print (list1 + tinylist)  # ['abcd', 786, 2.23, 'runoob', 70.2, 123, 'runoob']

# 元素可改变
list1[2:5] = [13, 14, 15]
print(list1)     # ['abcd', 786, 13, 14, 15]

list1[2:5] = [] # 将对应的元素值设置为 []
print(list1)   # ['abcd', 786]


# 截取第三个参数为负数，表示逆向读取
def reverseWords(input):
    
    # 通过空格将字符串分隔符，把各个单词分隔为列表
    inputWords = input.split(" ")
 
    # 翻转字符串
    # 假设列表 list = [1,2,3,4],  
    # list[0]=1, list[1]=2 ，而 -1 表示最后一个元素 list[-1]=4 ( 与 list[3]=4 一样)
    # inputWords[-1::-1] 有三个参数
    # 第一个参数 -1 表示最后一个元素
    # 第二个参数为空，表示移动到列表末尾
    # 第三个参数为步长，-1 表示逆向
    inputWords=inputWords[-1::-1]
 
    # 重新组合字符串
    output = ' '.join(inputWords)
     
    return output
 
if __name__ == "__main__":
    input = 'I like runoob'
    rw = reverseWords(input)
    print(rw)   # runoob like I

# 删除列表元素
list1 = [1,2,3,4,5]
del list1[2];
print(list1)   # [1,2,4,5]

# 长度
print(len(list1))   # 4

# 连接
print([1, 2, 3] + [4, 5, 6])    # [1,2,3,4,5,6]

# 循环
for x in [1, 2, 3]: print(x, end=" ")   # 1 2 3

# 列表比较
a = [1, 2]
b = [2, 3]
c = [2, 3]
print(operator.eq(a,b))   # False
print(operator.eq(c,b))   # True

# max
print(max([1,5,6]))   # 6

# min
print(min([1,5,6]))   # 1

# 元组转列表
print(list((1,2,3)))   # [1,2,3]
print(list("Hello"))   # ['H','e','l','l','o']

# 列表末尾添加元素
list1 = [1]
list1.append(2)    
print(list1)          # [1,2]

# 计算出现次数
print(list1.count(1))    # 1

# 连接两个列表
list1 = [1]
list1.extend([2])
print(list1)   # [1,2]

# 查找索引
print(list1.index(1))   # 0

# 插入
list1.insert(0, 0);
print(list1)   # [0, 1, 2]

# 移除，默认最后一个
list1.pop(0)
print(list1)   # [1,2]

# 移除列表中的某个值的第一个匹配项
list1  = [1,1,12]
list1.remove(1)
print(list1)    # [1, 12]

# 反向
list1.reverse()
print(list1)   # [12, 1]

# 排序， list.sort( key=None, reverse=False)
# key -- 主要是用来进行比较的元素，只有一个参数，具体的函数的参数就是取自于可迭代对象中，指定可迭代对象中的一个元素来进行排序
# reverse -- 排序规则，reverse = True 降序， reverse = False 升序（默认）

# 获取列表的第二个元素
def takeSecond(elem):
    return elem[1]

random = [(2, 2), (3, 4), (4, 1), (1, 3)]
random.sort(key=takeSecond, reverse = True)
print(random)   # [(3, 4), (1, 3), (2, 2), (4, 1)] 

# 清空列表
list1.clear()
print(list1)    # []

# 复制列表
list1 = [1,2]
list2 = list1.copy();
print(list2)   # [1,2]
