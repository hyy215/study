# 元组
# 元组（tuple）与列表类似，不同之处在于元组的元素不能修改。元组写在小括号 () 里，元素之间用逗号隔开

tuple1 = ( 'abcd', 786 , 2.23, 'runoob', 70.2  )
tinytuple = (123, 'runoob')

# 元组与字符串类似，可以被索引且下标索引从0开始，-1 为从末尾开始的位置。也可以进行截取
# 其实，可以把字符串看作一种特殊的元组
print (tuple1)             # ('abcd', 786 , 2.23, 'runoob', 70.2)
print (tuple[0])          # 'abcd'
print (tuple[1:3])        # (786 , 2.23)
print (tuple[2:])         # (2.23, 'runoob', 70.2)
print (tinytuple * 2)     # (123, 'runoob', 123, 'runoob')
print (tuple1 + tinytuple) # ('abcd', 786, 2.23, 'runoob', 70.2, 123, 'runoob')

# 虽然tuple的元素不可改变，但它可以包含可变的对象，比如list列表。
# 构造包含 0 个或 1 个元素的元组比较特殊，所以有一些额外的语法规则：

# 空元组
tup1 = () 
print(tup1)   # ()

# 一个元素，需要在元素后添加逗号，以区分它是一个元组而不是一个普通的值
# 因为没有逗号的情况下，Python 会将括号解释为数学运算中的括号，而不是元组的表示
tup2 = (20,) 
print(tup2)   # (20,)
not_a_tuple = (42)
print(not_a_tuple)   # 42

# 删除元祖
tup = (1,2)
del tup

# 列表转元组
list = [1,2]
tup = tuple(list)
print(tup)  # (1,2)

# 元组是不可变的
# tup[0] = 1; error