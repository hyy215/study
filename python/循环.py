# while 1
n = 10
sum = 0
counter = 1

while counter <= n:
    sum = sum + counter
    counter += 1
else:
    print('退出')

print("1 到 %d 之和为: %d" % (n,sum))

# 无限循环
# var = 1
# while var == 1 : # 表达式永远为 true
#    num = int(input("输入一个数字  :"))
#    print ("你输入的数字是: ", num)
 
# print ("Good bye!")

# for
sites = ["Baidu", "Google","Runoob","Taobao"]
for site in sites:
    print(site)
    # break 语句用于跳出当前循环体
    # continue
else :
     # 循环结束后执行的代码
    print('不在')

# range() 函数
for i in range(0, 10, 3):
    print(i)
# 0
# 3
# 6
# 9

# 结合 range() 和 len() 函数
a = ['Google', 'Baidu', 'Runoob', 'Taobao', 'QQ']
for i in range(len(a)):
    print(i, a[i])

# pass是空语句，是为了保持程序结构的完整性。
# pass 不做任何事情，一般用做占位语句，如下实例
for letter in 'Runoob': 
   if letter == 'o':
      pass
      print ('执行 pass 块')
   print ('当前字母 :', letter)
 
print ("Good bye!")