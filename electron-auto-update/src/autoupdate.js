const { dialog } = require('electron')
const { autoUpdater } = require('electron-updater')
const log = require('electron-log')
const fse = require('fs-extra')
const { downloadFile } = require('./download')
const AdmZip = require('adm-zip')
const path = require('path');

let app;

const config = {
    'upDateUrl': 'http://127.0.0.1:8081/app.zip',
    'upDateExe': 'http://127.0.0.1:8081/update.exe',
}

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
autoUpdater.autoDownload = false;
autoUpdater.setFeedURL('http://127.0.0.1:8081/');

// 有新版本
autoUpdater.on('update-available', async (info) => {
    const buttonIndex = await dialog.showMessageBox({
        type: 'info',
        title: '发现新版本',
        message: '是否更新',
        buttons: ['是', '否']
    })

    if (buttonIndex.response === 0) {
        const resourcesPath = process.resourcesPath;
        // 下载 update.exe
        if (!fse.pathExistsSync(path.join(app.getPath('userData'), './update.exe'))) {
            await downloadFile({ url: config.upDateExe, targetPath: app.getPath('userData') })
        }

        // 下载 app.zip
        downloadFile({ url: config.upDateUrl, targetPath: resourcesPath }).then(async (filePath) => {
            const zip = new AdmZip(filePath)
            zip.extractAllToAsync(resourcesPath, true, (err) => {
                if (err) {
                    console.error(err)
                    return
                }
                fse.removeSync(filePath)
                app.exit(0)
            })
        }).catch(err => {
            console.log(err)
        })
    }
})

// 不存在新版本
autoUpdater.on('update-not-available', async (info) => {
    dialog.showMessageBox({
        title: 'HYY TEST',
        message: '当前版本已为最新版本'
    })
})

// 存在错误
autoUpdater.on('error', (err) => {
    dialog.showErrorBox('更新检查失败: ', error == null ? "unknown" : (error.stack || error).toString())
})

function checkForUpdates(electronApp) {
    app = electronApp;
    autoUpdater.checkForUpdates()
}
module.exports.checkForUpdates = checkForUpdates